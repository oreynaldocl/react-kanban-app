import React from 'react';
import classnames from 'classnames';

class Edit extends React.Component {
  static defaultProps = {
    onEdit: () => { },
  }

  checkEnter = (e) => {
    if (e.key === 'Enter') {
      this.finishEdit(e);
    }
  }

  finishEdit = (e) => {
    const value = e.target.value;

    this.props.onEdit(value);
  }

  render() {
    const { value, onEdit, className, ...props } = this.props;
    return <input
      type="text"
      className={classnames('edit', className)}
      autoFocus={true}
      defaultValue={value}
      onBlur={this.finishEdit}
      onKeyPress={this.checkEnter}
      {...props}
    />;
  }
}

function Value({ value, className, ...props }) {
  return <span className={classnames('value', className)} {...props}>{value}</span>;
}

Editable.Edit = Edit;
Editable.Value = Value;

function Editable({ editing, value, onEdit, className, ...props }) {
  if (editing) {
    return <Editable.Edit
      className={className}
      value={value}
      onEdit={onEdit}
      {...props}
    />;
  }
  return <Editable.Value value={value} className={className} {...props} />;
}

export default Editable;
