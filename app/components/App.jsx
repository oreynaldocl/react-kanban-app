import React, { Component } from 'react';
import uuid from 'uuid';
import { compose } from 'redux';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import connect from '../libs/connect';

import Lanes from './Lanes';
import LaneActions from '../actions/LaneActions';

class App extends Component {
  addLane = () => {
    this.props.LaneActions.create({
      id: uuid.v4(),
      name: 'New lane',
    });
  };

  render() {
    const { lanes } = this.props;
    return (
      <div>
        <button className="add-lane" onClick={this.addLane}>+</button>
        <Lanes lanes={lanes} />
      </div>
    );
  }
}

const state = ({ lanes }) => (
  { lanes }
);
const actions = { LaneActions };

export default compose(
  DragDropContext(HTML5Backend),
  connect(state, actions),
)(App);
