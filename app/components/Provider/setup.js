
import NoteStore from '../../stores/NoteStore';
import storage from '../../libs/storage';
import persist from '../../libs/persist';
import LaneStore from '../../stores/LaneStore';

function setup(alt) {
  alt.addStore('NoteStore', NoteStore);
  alt.addStore('LaneStore', LaneStore);

  persist(alt, storage(localStorage), 'app');
}

export default setup;
