import React from 'react';
import { compose } from 'redux';
import { DropTarget } from 'react-dnd';

import ItemTypes from '../constants/itemTypes';
import connect from '../libs/connect';
import NoteActions from '../actions/NoteActions';
import LaneActions from '../actions/LaneActions';
import Notes from './Notes';
import LaneHeader from './LaneHeader';

function Lane({ connectDropTarget, lane, notes, LaneActions, NoteActions, ...props }) {
  const editNote = (id, task) => {
    NoteActions.update({ id, task, editing: false });
  };

  const deleteNote = (noteId, e) => {
    e.stopPropagation();

    LaneActions.detachFromLane({
      laneId: lane.id,
      noteId,
    });

    NoteActions.delete(noteId);
  };

  const activateNoteEdit = id => {
    NoteActions.update({ id, editing: true });
  };

  return connectDropTarget(
    <div {...props}>
      <LaneHeader lane={lane} />
      <Notes
        notes={selectNotesByIds(notes, lane.notes)}
        onNoteClick={activateNoteEdit}
        onEdit={editNote}
        onDelete={deleteNote} />
    </div>
  );
}

function selectNotesByIds(allNotes, noteIds = []) {
  const mapIds = noteIds.reduce((res, noteId) => ({ ...res, [noteId]: true }), {});
  return noteIds.length === 0 ? [] : allNotes.filter(note => mapIds[note.id]);
}

const noteTarget = {
  hover(targetProps, monitor) {
    const sourceProps = monitor.getItem();
    const sourceId = sourceProps.id;

    // If the target lane doesn't have notes,
    // attach the note to it.
    //
    // `attachToLane` performs necessarly
    // cleanup by default and it guarantees
    // a note can belong only to a single lane
    // at a time.
    if (!targetProps.lane.notes.length) {
      LaneActions.attachToLane({
        laneId: targetProps.lane.id,
        noteId: sourceId,
      });
    }
  },
};

const DefineDrag = DropTarget(ItemTypes.NOTE, noteTarget, connect => ({
  connectDropTarget: connect.dropTarget(),
}));

const state = ({ notes }) => (
  { notes }
);
const actions = { NoteActions, LaneActions };

export default compose(DefineDrag, connect(state, actions))(Lane);
