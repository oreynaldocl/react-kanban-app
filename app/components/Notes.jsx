import React from 'react';

import Note from './Note';
import Editable from './Editable';
import LaneActions from '../actions/LaneActions';

const defFunc = () => { };

function Notes({ notes, onNoteClick = defFunc, onEdit = defFunc, onDelete = defFunc }) {
  return (
    <ul className="notes">
      {notes.map(({ id, task, editing }) =>
        <li key={id}>
          <Note className="note" id={id}
            editing={editing}
            onClick={onNoteClick.bind(null, id)}
            onMove={LaneActions.move}
          >
            <Editable
              className="editable"
              editing={editing}
              value={task}
              onEdit={onEdit.bind(null, id)}
            />
            <button
              className="delete"
              onClick={onDelete.bind(null, id)}>x</button>
          </Note>
        </li>
      )}
    </ul>
  );
}

export default Notes;
