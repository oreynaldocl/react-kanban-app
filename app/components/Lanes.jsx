import React from 'react';
import Lane from './Lane';

function Lanes({ lanes }) {
  return (
    <div className="lanes">
      {lanes.map(lane =>
        <Lane className="lane" key={lane.id} lane={lane} />
      )}
    </div>
  );
}

export default Lanes;
