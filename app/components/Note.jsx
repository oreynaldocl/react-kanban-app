import React from 'react';
import { compose } from 'redux';
import { DragSource, DropTarget } from 'react-dnd';

import ItemTypes from '../constants/itemTypes';

function Note({ connectDragSource, connectDropTarget, isDragging,
  isOver, onMove, id, editing, children, ...props }) {
  const opacity = isDragging || isOver ? 0 : 1;
  const dragSource = editing ? a => a : connectDragSource;

  return compose(dragSource, connectDropTarget)(
    <div style={{ opacity }} {...props}>
      {children}
    </div>
  );
}

const noteSource = {
  beginDrag({ id }) {
    return { id };
  },
};

const noteTarget = {
  hover(targetProps, monitor) {
    const targetId = targetProps.id;
    const sourceProps = monitor.getItem();
    const sourceId = sourceProps.id;

    if (sourceId !== targetId) {
      targetProps.onMove({ sourceId, targetId });
    }
  },
};

const DefineDrag = DragSource(ItemTypes.NOTE, noteSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}));
const DefineDrop = DropTarget(ItemTypes.NOTE, noteTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
}));

export default compose(DefineDrag, DefineDrop)(Note);
