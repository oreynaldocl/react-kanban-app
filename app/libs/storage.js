function buildStorage(storage) {
  return {
    get(k) {
      try {
        return JSON.parse(storage.getItem(k));
      }
      catch (e) {
        console.log('error', e);
        return null;
      }
    },

    set(k, v) {
      storage.setItem(k, JSON.stringify(v));
    },
  };
}

export default buildStorage;
